/**
 *
 */
package massi.gerardi.goeurotest.services;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import massi.gerardi.goeurotest.cvs.services.CSVService;
import massi.gerardi.goeurotest.cvs.services.CSVServiceException;
import massi.gerardi.goeurotest.goeuro.model.Location;
import massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService;
import massi.gerardi.goeurotest.goeuro.services.GoEuroLocationServiceException;

/**
 * The main service.
 *
 * @author Massimiliano Gerardi
 *
 */
@Service
public class GoEuroTestService {

    private final static String[] FIELDS = new String[] { "id", "name", "type", "position.latitude", "position.longitude"};
    private final static String[] HEADERS = new String[] { "_id", "name", "type", "latitude", "longitude"};

    @Autowired
    private GoEuroLocationService geoService;

    @Autowired
    private CSVService csvService;

    public File writeCsvFor(final String locationName) throws GoEuroTestServiceException {
        try {
            final List<Location> locations = geoService.getLocation(locationName);
            return csvService.writeFile(locations, locationName, FIELDS, HEADERS);
        } catch (final GoEuroLocationServiceException | CSVServiceException | URISyntaxException e) {
            throw new GoEuroTestServiceException(e);
        }
    }

}
