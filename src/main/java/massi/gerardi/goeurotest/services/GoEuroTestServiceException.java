/**
 *
 */
package massi.gerardi.goeurotest.services;

/**
 * @author Massimiliano Gerardi
 *
 */
public class GoEuroTestServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8407418634404845458L;

    /**
     *
     */
    public GoEuroTestServiceException() {
        super();
    }

    /**
     * @param message
     */
    public GoEuroTestServiceException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public GoEuroTestServiceException(final Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public GoEuroTestServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public GoEuroTestServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
