/**
 *
 */
package massi.gerardi.goeurotest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * The main Spring Configuration class.
 *
 * @author Massimiliano Gerardi
 *
 */
@Configuration
@ComponentScan({
    "massi.gerardi.goeurotest"
})
public class GoEuroTestConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
