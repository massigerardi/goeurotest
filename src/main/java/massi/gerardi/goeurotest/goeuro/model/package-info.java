/**
 * The domain objects used to represent a location and its position.
 *
 * @author Massimiliano Gerardi
 *
 */
package massi.gerardi.goeurotest.goeuro.model;