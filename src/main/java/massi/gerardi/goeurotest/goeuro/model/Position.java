/**
 *
 */
package massi.gerardi.goeurotest.goeuro.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * A simple Geo Position.
 *
 * @author Massimiliano Gerardi
 *
 */
@Getter
@ToString(includeFieldNames = false)
@EqualsAndHashCode
public class Position {

    private double longitude;

    private double latitude;

}
