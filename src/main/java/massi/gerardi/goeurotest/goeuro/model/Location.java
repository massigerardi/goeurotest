/**
 *
 */
package massi.gerardi.goeurotest.goeuro.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * A simple location.
 *
 * @author Massimiliano Gerardi
 *
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(includeFieldNames = false)
@EqualsAndHashCode
public class Location {

    @JsonProperty("_id")
    private String id;

    private String name;

    private String type;

    @JsonProperty("geo_position")
    private Position position;
}
