/**
 *
 */
package massi.gerardi.goeurotest.goeuro.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import massi.gerardi.goeurotest.goeuro.model.Location;

/**
 * A service to retrieve the locations description from the remote server.
 *
 * Use a Spring Rest Template to query the remote server.
 *
 * @author Massimiliano Gerardi
 *
 */
@Service
public class GoEuroLocationService {

    @Autowired
    private RestTemplate template;

    public final static String BASE_URL = "http://api.goeuro.com/api/v2/position/suggest/en/%s";

    /**
     * Retrieves the location descriptions.
     *
     * @param name the location's name
     * @return a list of locations
     * @throws GoEuroLocationServiceException if the name is null or blank, or when an error occurs.
     */
    public List<Location> getLocation(final String name) throws URISyntaxException, GoEuroLocationServiceException {
        if (StringUtils.isBlank(name)) {
            throw new GoEuroLocationServiceException("Blank Location");
        }
        final String url =  String.format(BASE_URL, name);
        final URI uri = new URI(url);
        final ResponseEntity<Location[]> locations = template.getForEntity(uri, Location[].class);
        return Lists.newArrayList(locations.getBody());
    }

}
