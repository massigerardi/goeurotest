/**
 *
 */
package massi.gerardi.goeurotest.goeuro.services;

/**
 * An Exception container for the Service.
 *
 * @author Massimiliano Gerardi
 *
 */
public class GoEuroLocationServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -836893952397652117L;

    public GoEuroLocationServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public GoEuroLocationServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GoEuroLocationServiceException(final String message) {
        super(message);
    }

    public GoEuroLocationServiceException(final Throwable cause) {
        super(cause);
    }

    public GoEuroLocationServiceException() {
        super();
    }

}
