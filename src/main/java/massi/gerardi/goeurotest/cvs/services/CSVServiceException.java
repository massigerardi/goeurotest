/**
 *
 */
package massi.gerardi.goeurotest.cvs.services;

/**
 * An Exception container for CSV Service
 *
 * @author Massimiliano Gerardi
 *
 */
public class CSVServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -5946332643784751559L;

    /**
     *
     */
    public CSVServiceException() {
        super();
    }

    /**
     * @param message
     */
    public CSVServiceException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public CSVServiceException(final Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public CSVServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public CSVServiceException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
