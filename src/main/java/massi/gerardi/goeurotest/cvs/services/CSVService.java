package massi.gerardi.goeurotest.cvs.services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.springframework.stereotype.Service;
import org.supercsv.io.dozer.CsvDozerBeanWriter;
import org.supercsv.io.dozer.ICsvDozerBeanWriter;
import org.supercsv.prefs.CsvPreference;

import lombok.extern.slf4j.Slf4j;
import massi.gerardi.goeurotest.goeuro.model.Location;

/**
 *
 */

/**
 * A service to generate CSV files, using Dozer.
 *
 *
 * @author Massimiliano Gerardi
 *
 */
@Slf4j
@Service
public class CSVService {

    /**
     * Writes a list of {@link Location} in a csv files.
     *
     * @param locations the list of locatio
     * @param name the name for the file
     * @param fields the fields to be used to fill the csv columns
     * @param headers the headers of the csv columns
     * @return a file with extension .csv
     * @throws CSVServiceException
     */
    public File writeFile(final List<Location> locations, final String name, final String[] fields, final String[] headers) throws CSVServiceException {
        final File file = new File(String.format("%s.csv", name));
        Writer fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
        } catch (final IOException e) {
            throw new CSVServiceException(e);
        }
        final ICsvDozerBeanWriter beanWriter = new CsvDozerBeanWriter(fileWriter, CsvPreference.STANDARD_PREFERENCE);
        try {
            beanWriter.writeHeader(headers);
            beanWriter.configureBeanMapping(Location.class, fields);
            locations.forEach(t -> {
                try {
                    beanWriter.write(t);
                } catch (final Exception e) {
                    log.error("while writing "+t, e);
                }
            });
            beanWriter.flush();
            return file;
        } catch (final IOException e) {
            throw new CSVServiceException(e);
        } finally {
            try {
                beanWriter.close();
            } catch (final IOException e) {
                log.error("Closing CsvDozerBeanWriter", e);
            }
        }

    }

}
