/**
 *
 */
package massi.gerardi;

import java.io.File;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import lombok.extern.slf4j.Slf4j;
import massi.gerardi.goeurotest.configuration.GoEuroTestConfiguration;
import massi.gerardi.goeurotest.services.GoEuroTestService;
import massi.gerardi.goeurotest.services.GoEuroTestServiceException;

/**
 * The main application.
 *
 * It loads all the services using Spring Application Context with annotated classes.
 *
 * @author Massimiliano Gerardi
 *
 */
@Slf4j
public class MainApplication {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        if (args.length != 1) {
            log.warn("Invalid input, please specify a location");
            return;
        }
        run(args[0]);
    }

    public static void run(final String location) {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(GoEuroTestConfiguration.class);
        final GoEuroTestService service = context.getBean(GoEuroTestService.class);
        try {
            final File csv = service.writeCsvFor(location);
            log.info("File for location '{}' created at {}", location, csv.getAbsolutePath());
        } catch (final GoEuroTestServiceException e) {
            log.error("Error for location "+location , e);
        }
        context.close();
    }

}
