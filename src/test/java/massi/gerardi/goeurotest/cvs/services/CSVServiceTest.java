/**
 *
 */
package massi.gerardi.goeurotest.cvs.services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import junitx.framework.FileAssert;
import massi.gerardi.ServiceTest;
import massi.gerardi.goeurotest.goeuro.model.Location;

/**
 * @author Massimiliano Gerardi
 *
 */
public class CSVServiceTest extends ServiceTest {

    CSVService service = new CSVService();

    /**
     * Test method for {@link massi.gerardi.goeurotest.cvs.services.CSVService#generate(java.util.List, java.lang.String)}.
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @Test
    public void testGenerate() throws Exception {
        testCsv("berlin");
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.cvs.services.CSVService#generate(java.util.List, java.lang.String)}.
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @Test
    public void testGenerateEmpty() throws Exception {
        testCsv("empty");
    }

    private void testCsv(final String location) throws Exception {
        final List<Location> locations = getLocations(location);
        final File file = service.writeFile(locations, location, FIELDS, HEADERS);
        Assert.assertNotNull(file);
        Assert.assertTrue(file.exists());
        final File expected = new File(String.format("src/test/resources/%s.csv", location));
        FileAssert.assertEquals(expected, file);
        file.deleteOnExit();
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.cvs.services.CSVService#generate(java.util.List, java.lang.String)}.
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @Test(expected=IllegalArgumentException.class)
    public void testGenerateEmptyHeaders() throws Exception {
        final List<Location> locations = getLocations("berlin");
        service.writeFile(locations, "empty", FIELDS, new String[] {});
    }

}
