/**
 *
 */
package massi.gerardi.goeurotest.services;

import java.io.File;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import massi.gerardi.goeurotest.cvs.services.CSVService;
import massi.gerardi.goeurotest.cvs.services.CSVServiceException;
import massi.gerardi.goeurotest.goeuro.model.Location;
import massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService;
import massi.gerardi.goeurotest.goeuro.services.GoEuroLocationServiceException;

/**
 * @author Massimiliano Gerardi
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GoEuroTestServiceTest {

    @Mock
    private CSVService csvService;

    @Mock
    private GoEuroLocationService geoService;

    @Mock
    private List<Location> locations;

    @Mock
    private File file;

    @InjectMocks
    private GoEuroTestService service;

    /**
     * Test method for {@link massi.gerardi.goeurotest.services.GoEuroTestService#writeCsvFor(java.lang.String)}.
     */
    @Test
    public void testWriteCsvFor() throws Exception {
        Mockito.when(geoService.getLocation(Matchers.anyString())).thenReturn(locations);
        Mockito.when(csvService.writeFile(Matchers.anyList(), Matchers.anyString(), (String[]) Matchers.notNull(), (String[]) Matchers.notNull()))
            .thenReturn(file);
        Assert.assertNotNull(service.writeCsvFor("berlin"));
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.services.GoEuroTestService#writeCsvFor(java.lang.String)}.
     */
    @Test
    public void testWriteCsvForNull() throws Exception {
        Mockito.when(geoService.getLocation(Matchers.anyString())).thenReturn(locations);
        Mockito.when(csvService.writeFile(Matchers.anyList(), Matchers.anyString(), (String[]) Matchers.notNull(), (String[]) Matchers.notNull()))
            .thenReturn(null);
        Assert.assertNull(service.writeCsvFor("berlin"));
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.services.GoEuroTestService#writeCsvFor(java.lang.String)}.
     */
    @Test(expected=GoEuroTestServiceException.class)
    public void testWriteCsvForwWithGeoException() throws Exception {
        Mockito.when(geoService.getLocation(Matchers.anyString())).thenThrow(new GoEuroLocationServiceException());
        service.writeCsvFor("berlin");
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.services.GoEuroTestService#writeCsvFor(java.lang.String)}.
     */
    @Test(expected=GoEuroTestServiceException.class)
    public void testWriteCsvForwWithCsvException() throws Exception {
        Mockito.when(csvService.writeFile(Matchers.anyList(), Matchers.anyString(), (String[]) Matchers.notNull(), (String[]) Matchers.notNull()))
            .thenThrow(new CSVServiceException());
        service.writeCsvFor("berlin");
    }

}
