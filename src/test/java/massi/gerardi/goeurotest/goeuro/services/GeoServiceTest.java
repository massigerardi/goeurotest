/**
 *
 */
package massi.gerardi.goeurotest.goeuro.services;

import java.net.URI;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import massi.gerardi.ServiceTest;
import massi.gerardi.goeurotest.goeuro.model.Location;

/**
 * @author Massimiliano Gerardi
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GeoServiceTest extends ServiceTest {

    @Mock
    private RestTemplate template;

    @Mock
    private ResponseEntity<Location[]> entity;

    @InjectMocks
    private GoEuroLocationService service;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.when(template.getForEntity(Matchers.any(URI.class), (Class<Location[]>) Matchers.notNull())).thenReturn(entity);
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService#getLocation(java.lang.String)}.
     */
    @Test
    public void testGetLocationBerlin() throws Exception {
        Mockito.when(entity.getBody()).thenReturn(getLocationsAsArray("berlin"));
        final List<Location> locations = service.getLocation("berlin");
        Assert.assertNotNull(locations);
        Assert.assertFalse(CollectionUtils.isEmpty(locations));
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService#getLocation(java.lang.String)}.
     */
    @Test
    public void testGetLocationRome() throws Exception {
        Mockito.when(entity.getBody()).thenReturn(getLocationsAsArray("empty"));
        final List<Location> locations = service.getLocation("empty");
        Assert.assertNotNull(locations);
        Assert.assertTrue(CollectionUtils.isEmpty(locations));
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService#getLocation(java.lang.String)}.
     */
    @Test(expected=GoEuroLocationServiceException.class)
    public void testGetLocationNull() throws Exception {
        service.getLocation(null);
    }

    /**
     * Test method for {@link massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService#getLocation(java.lang.String)}.
     */
    @Test(expected=GoEuroLocationServiceException.class)
    public void testGetLocationBlank() throws Exception {
        service.getLocation(" ");
    }


}
