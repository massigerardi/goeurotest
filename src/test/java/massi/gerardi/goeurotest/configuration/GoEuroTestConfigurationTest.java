/**
 *
 */
package massi.gerardi.goeurotest.configuration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.RestTemplate;

import massi.gerardi.goeurotest.cvs.services.CSVService;
import massi.gerardi.goeurotest.goeuro.services.GoEuroLocationService;
import massi.gerardi.goeurotest.services.GoEuroTestService;

/**
 * @author Massimiliano Gerardi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=GoEuroTestConfiguration.class, loader=AnnotationConfigContextLoader.class)
public class GoEuroTestConfigurationTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CSVService csvService;

    @Autowired
    private GoEuroLocationService geoService;

    @Autowired
    private GoEuroTestService goEuroTestService;

    @Test
    public void testCSVService() {
        Assert.assertNotNull(csvService);
    }

    @Test
    public void testRestTemplate() {
        Assert.assertNotNull(restTemplate);
    }

    @Test
    public void testGeoService() {
        Assert.assertNotNull(geoService);
    }

    @Test
    public void testGoEuroTestService() {
        Assert.assertNotNull(goEuroTestService);
    }

}
