/**
 *
 */
package massi.gerardi;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import junitx.framework.FileAssert;

/**
 * @author Massimiliano Gerardi
 *
 */
public class MainApplicationTest extends ServiceTest {

    /**
     * Test method for {@link massi.gerardi.MainApplication#main(java.lang.String[])}.
     */
    @Test
    public void testMain() throws Exception {
        testCsv("berlin");
    }

    private void testCsv(final String location) throws Exception {
        MainApplication.main(new String[] {location});
        final File file = new File(String.format("%s.csv", location));
        Assert.assertNotNull(file);
        Assert.assertTrue(file.exists());
        final File expected = new File(String.format("src/test/resources/%s.csv", location));
        FileAssert.assertEquals(expected, file);
        file.deleteOnExit();
    }

}
