/**
 *
 */
package massi.gerardi;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import massi.gerardi.goeurotest.goeuro.model.Location;

/**
 * @author Massimiliano Gerardi
 *
 */
public abstract class ServiceTest {

    protected final static String[] FIELDS = new String[] { "id", "name", "type", "position.latitude", "position.longitude"};
    protected final static String[] HEADERS = new String[] { "_id", "name", "type", "latitude", "longitude"};

    private final ObjectMapper mapper = new ObjectMapper();

    protected List<Location> getLocations(final String name) throws JsonParseException, JsonMappingException, IOException {
        final InputStream berlinIS = ClassLoader.class.getResourceAsStream(String.format("/%s.json", name));
        final TypeReference<List<Location>> reference = new TypeReference<List<Location>>() {};
        return mapper.readValue(berlinIS, reference);
    }


    protected Location[] getLocationsAsArray(final String name) throws Exception {
        return getLocations(name).toArray(new Location[] {});
    }



}
