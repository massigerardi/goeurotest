# GoEuro Test

A Solution to entry level test for GoEuro.
It uses:

- [Spring](http://spring.io/)
- [Super CSV Dozer extension](https://super-csv.github.io/super-csv/dozer.html)
- [Lombok](https://projectlombok.org/)
- [Google Guava](https://github.com/google/guava)
- [Apache Commons](https://commons.apache.org/)
- [Mockito](http://mockito.org/)

## Use

Create the jar using the maven command:

```
mvn clean package
```

Run the application by giving the command:

```
java -jar target/GoEuroTest.jar CITY_NAME
```

where `CITY_NAME` is the searched location.

## Output

If no `CITY_NAME` has been passed:

```
java -jar target/GoEuroTest.jar
WARN  MainApplication:32 - Invalid input, please specify a location
```

otherwise:

```
java -jar target/GoEuroTest.jar CITY_NAME
INFO  MainApplication:44 - File for location 'CITY_NAME' created at <full-path-to/CITY_NAME.csv>
```

The csv file generated is provided with headers. In case of empty json result, an empty file with just headers is generated.
It has been assumed that an external application calling the service would expect not a null csv file.

## JUnit Test Coverage

To see the JUnit test coverage in `target/site/cobertura/index.html`, run the maven command:

```
mvn cobertura:cobertura
```

JUnit tests have been provided for main services and configuration. No test has been provided for beans, simplest classes (i.e. Exceptions) and methods that have been generated (i.e. Lombok `@EqualsAndHashCode`)

A Junit coverage of 77% has been reached.

### Note

An exception will be displayed while generating the Cobertura report: this is due to the fact that Cobertura does not parse correctly Java classes that use lambda functions.
This does not affect the analysis.



